#include <stdio.h>
#include <stdlib.h>

//Función punto de entrada
int main(){

    int lado;
    char respuesta;

    do {
        system("clear");
        printf("Inserte numero de lado: ");
        scanf(" %i", &lado);
        printf(" \n");
        for(int fila=0; fila<lado; fila++){
            for (int col=0; col<lado; col++)
                if (fila==0 || col==0 || fila == lado-1 || col == lado-1 || col==fila || (col+fila)==lado-1)
                    printf(" *");
                else
                    printf("  ");

            printf("\n");
        }
        printf("\nPulsa s para volver al principio: ");
        scanf(" %c", &respuesta);

    }
    while (respuesta == 's');
    system("clear");
    printf("Bye\n");

    return EXIT_SUCCESS;
}
