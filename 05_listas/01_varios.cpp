#include <stdio.h>
#include <stdlib.h>

//Función punto de entrada
int main(){

    unsigned primo[] = {2, 3, 5, 7, 11, 13, 17, 19, 23}; //No tiene signo, es positivo
    unsigned elementos = (unsigned) sizeof(primo) / sizeof(int);
    unsigned *peeping = primo;
    char *tom = (char *) primo;

    //Police contiene la direccion (primo) de la direccion de un entero {2, 3, 5...}
    unsigned **police = &peeping;
    printf( "PRIMO:\n"
            "======\n"
            " Localización (%p)\n"
            " Elementos: %u [%u..%u]\n"
            " Tamaño: %lu bytes.\n\n",
            primo, elementos, primo[0], primo[elementos-1], sizeof(primo)); //%u de unsigned (un entero)
    printf("0: %u\n", peeping[0] ); //anotacion de matrices
    printf("1: %u\n", peeping[1] );
    printf("0: %u\n", *peeping); //anotacion de punteros
    printf("1: %u\n", *(peeping+1) );
    printf("Tamaño: %lu bytes.\n", sizeof(peeping) );
    printf("\n");

    /* Memory Dump */
    for (int i=0; i<sizeof(primo); i++)
        printf("%02X", *(tom + i));
    printf("\n\n");

    printf("Police contiene %p\n", police); //lo que contiene police
    printf("Primo contiene %p\n", *police); //lo que tiene alli donde apunta (en peeping)
    printf("Primo[0] contiene %u\n", **police); //alli donde apunta police (es donde apunta peeping)


    return EXIT_SUCCESS;
}
