#include <stdlib.h>
#include <stdio.h>

#define N 15

enum {num, den};

int divisores(int numerador, int lista[N]) {
    int divisores = 0;
    for (int pd=numerador/2; pd>1; pd--)
        if (numerador % pd == 0)
            lista[divisores++] = pd;

    return divisores;
}

bool esta(int buscado, int lista[N]) {
    bool encontrado = false;
    for (int i=0; i<N && !encontrado; i++)
        if (lista[i] == buscado)
            encontrado = true;
    return encontrado;
}

int main(){

    int div[2][N];
    int n_div[2] = {0, 0};
    int numerador = 78,
        denominador = 46;
    bool divisor_comun;

    do {
        n_div[num] = divisores(numerador, div[num]);   /* Calculamos los divisores del primero */
        n_div[den] = divisores(denominador, div[den]); /* Calculamos los divisores del segundo */
        printf ("Fracción original: \t%i / %i\n", numerador, denominador);
        /* Buscamos un divisor común */
        divisor_comun = false;
        for (int i=0; i<n_div[num] && !divisor_comun; i++)
            if (esta(div[num][i], div[den]) ){
                numerador /= div[num][i];
                denominador/= div[num][i];
                divisor_comun = true;
                /* Si lo hay: simplificamos y volvemos a empezar */
            }
    } while (divisor_comun == false);

    printf("Fracción simplificada: \t%i / %i", numerador, denominador);
    printf ("\n");

    return EXIT_SUCCESS;
}
