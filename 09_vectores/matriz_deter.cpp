#include <stdio.h>
#include <stdlib.h>

#define D 3

//Función punto de entrada
int main(){

    double a[D][D],
           multiplicacion,
           determinante;

    //ENTRADA DE DATOS
    for(int i=0;i<D;i++){
        printf("Dime el vector %i:\n", i + 1);
        scanf("%lf, %lf, %lf", &a[i][0], &a[i][1], &a[i][2]);
    }


    //CALCULO DE DATOS
    for(int f=0; f<D; f++){
      multiplicacion = 1;
      for (int d=0; d<D; d++)
          multiplicacion *= a[(f+d)%D][0+d];
      determinante += multiplicacion;
    }

//    resultado = (a[0][0]*a[1][1]*a[2][2])+
//                (a[1][0]*a[2][1]*a[0][2])+
//                (a[2][0]*a[0][1]*a[1][2]);


    //SALIDA DE DATOS
    printf("\nEl resultado es: %.2lf\n\n\n", determinante);


    //DATOS DEL ARRAY
    printf("Datos del array\n");
    for(int i=0;i<3;i++)
        printf("  %.2lf, %.2lf, %.2lf\n", a[i][0], a[i][1], a[i][2]);


    return EXIT_SUCCESS;

}
