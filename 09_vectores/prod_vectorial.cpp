#include <stdio.h>
#include <stdlib.h>

//Función punto de entrada
int main(){

    double ax = 3,
           ay = 2,
           bx = 4,
           by = 7,
           resultado;

    resultado = (ax*by)-(ay*bx);

    printf("El resultado es: 00%.2lf\n", resultado);
    return EXIT_SUCCESS;
}

//00,13
