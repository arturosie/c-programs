#include <stdio.h>
#include <stdlib.h>

#define BMP_NAME "panda.bmp"
#define WIDTH 1192L
#define HEIGHT 984L
#define IMAGE_OFFSET 0x8A

//#define BMP_NAME "actress.bmp"
//#define WIDTH 200
//#define HEIGHT 200
//#define IMAGE_OFFSET 0x36

#define Bpp   3
#define ROW_SIZE (WIDTH * Bpp + 3 ) / 4 * 4

const char *two [] = { "█", " " };

const char * two_colors(
        unsigned char r,
        unsigned char g,
        unsigned char b,
        int intensidad){

    int media = (r +g + b)/3;
    return media > intensidad ? " " : "█";
}

int  main(int argc, char *argv[]){

    int intensidad = atoi(argv[1]);
    intensidad = (intensidad * 255)/100;
    unsigned char *image;
    image = (unsigned char *) malloc( HEIGHT * ROW_SIZE ) ;

    FILE *pf;
    if (!(pf = fopen (BMP_NAME, "r"))){
        fprintf (stderr, "Mac donde estas?\nRai no te veo.\n");
        return EXIT_FAILURE;
    }

    fseek (pf, IMAGE_OFFSET, SEEK_SET);
    fread (image, 1, HEIGHT * ROW_SIZE, pf);
    fclose (pf);

    for (int row=HEIGHT; row>=0; row-- ){
        for (int col=0; col<WIDTH; col++) {
            unsigned char * px = &image[ROW_SIZE * row + Bpp * col];
            printf ("%s", two_colors(
                        px[0],
                        px[1],
                        px[2],
                        intensidad 
                        ));
        }
        printf ("\n");
    }

    free (image);
    return EXIT_SUCCESS;
}
