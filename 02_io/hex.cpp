
#include <stdio.h>
#include <stdlib.h>

//Función punto de entrada
int main(){
    int numero;

    printf("Dime el numero en decimal: ");
    scanf(" %i", &numero);
    //printf(" 0x%X\n", &numero);
    printf("El numero en hexadecimal es 0x%X\n", numero);

    return EXIT_SUCCESS;
}
