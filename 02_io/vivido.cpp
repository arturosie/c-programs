#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada */
int  main(){

    int dn, mn, an; // dia, mes y año de nacimiento
    int dh, mh, ah; // d, m, a de hoy
    int dias;

    printf("Nacimiento (dd/mm/yyyy):");
    scanf(" %d/%d/%i", &dn, &mn, &an);
    printf("Hoy (dd/mm/yyyy): ");
    scanf(" %d/%d/%i", &dh, &mh, &ah);

    dias = dh - dn + (mh - mn) * 30 + (ah - an) * 365;
    printf("Has vivido %i dias.\n", dias);

    return EXIT_SUCCESS;
}
