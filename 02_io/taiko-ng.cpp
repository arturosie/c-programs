#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VECES 12
#define BTU 100000 /* Basic Time Unit*/

//Cancion creada con sonido aviso del sistema
//Función punto de entrada
int main(){
    int duracion[VECES] = {0, 2, 8, 2, 1, 8, 2, 1, 2, 1, 2, 2};

    //set sw=4 la tabulacion, cntrl = g para maquetar
    //bucle for; debe valer para repetir cosas
    for (int i=0; i<VECES; i++) {
	usleep(duracion[i] * BTU);
	fputc('\a', stderr);	
    }
    return EXIT_SUCCESS;
}
