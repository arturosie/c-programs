#include <stdio.h>
#include <stdlib.h>

#define VECES 10
#define MAX 0x100

void pon_titulo(int tabla){ /*Parametro formal*/ //VOID ES UNA FUNCION, la estamos declarando
	/*Definicion de la funcion*/
	char titulo[MAX];
	sprintf(titulo, "toilet -fpagga TABLA DEL %i", tabla); //imprime en memoria
	system(titulo);
}

//Función punto de entrada
int main(){

	/*Declaración de variables*/	
	int numerotabla;
//	int numero;
	int mult;

	system("clear");

	/*Menu*/
	system("toilet -fpagga LAS TABLAS DE MULTIPLICAR");
	printf("¿Que tabla de multiplicar quieres saber?\n");
	scanf("%i", &numerotabla);
	system("clear"); 

	/*Titulo*/
	pon_titulo(numerotabla); /*Llamada con parametro actual*/

	/*Resultado*/
	for (int i=1; i<=VECES; i++){
		mult=numerotabla*i;
		printf("%i x %i = %i\n", numerotabla, i, mult);
	}
	return EXIT_SUCCESS;
}
