#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//Cancion creada con sonido aviso del sistema
//Función punto de entrada
int main(){
    /* printf tiene escritura bufferizada */
    //Cambia la salida al tuvo de errores, porque lo he intidcado (no bufferizada) 
    /*fprintf(stderr, "\a");*/
    
    //La diferencia a fprintf es que el primero hace una cadena de caracteres y el otro un caracter 
    fputc('\a', stderr);
    usleep(100000); 
    printf("\a\n");
    
    return EXIT_SUCCESS;
}
