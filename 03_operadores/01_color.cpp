#include <stdio.h>
#include <stdlib.h>

#define NUM 3

//Función punto de entrada
int main(){
    int a = NUM;

    if (a ==2)
        printf("a vale 2.\n");
    else {
        printf("a vale distinto de 2.\n");
        printf("Y yo estoy mosqueado\n");
        puts("Adieu"); //Es un printf que no te deja hacer nada (pone \n al final)
    }
    return EXIT_SUCCESS;
}
