#include <stdio.h>
#include <stdlib.h>


/* Función punto de entrada */
int  main(){

    bool r = false, y = false, b = false;
    char respuesta;

    printf("Ves rojo? (s/N): ");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        r = true;

    printf("Ves amarillo? (s/n): ");
    scanf(" %c", &respuesta);
        y = respuesta == 's';

    printf("Ves azul? (s/n): ");
    scanf(" %c", &respuesta);
        b = respuesta == 's';

    if (r)
        if (y)
            if (b)
                printf("Blanco.\n");
            else
                printf("Naranja.\n");
        else
            if (b)
                printf("Morado.\n");
            else
                printf("Rojo.\n");
    else
        if (y)
            if (b)
                printf("Verde.\n");
            else
                printf("Amarillo.\n");
        else
            if (b)
                printf("Azul.\n");
            else
                printf("Negro.\n");


    return EXIT_SUCCESS;
}

