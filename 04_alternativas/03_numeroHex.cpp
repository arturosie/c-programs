#include <stdio.h>
#include <stdlib.h>

//Función punto de entrada
int main(){
  int hex;

  system("clear");
  printf("Dime tu número en hexadecimal: ");
  scanf(" %x", &hex);
  if (hex %3 == 0)
      printf("Es multiplo de 3\n");
  else
      printf("No es multiplo de 3\n");

    return EXIT_SUCCESS;
}
