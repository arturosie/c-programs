#include <stdio.h>
#include <stdlib.h>

#define ROJO 4
#define AMAR 2
#define AZUL 1

//Función punto de entrada
int main(){
    char respuesta;
    int color = 0;

    system("clear");
    
    /*Entrada de datos*/
    printf("Ves el color rojo (s/n): ");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color |= ROJO;

    printf("Ves el color amarillo (s/n): ");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color |= AMAR;

    printf("Ves el color azul (s/n): ");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color |= AZUL;

    system("clear");

    switch(color) {
        case 0:
            printf("Ves el color Negro\n");
            break;

        case 1:
            printf("Ves el color Azul\n");
            break;

        case 2:
            printf("Ves el color Amarillo\n");
            break;

        case 3:
            printf("Ves el color Verde\n");
            break;

        case 4:
            printf("Ves el color Rojo\n");
            break;

        case 5:
            printf("Ves el color Morado\n");
            break;

        case 6:
            printf("Ves el color Naranja\n");
            break;

        case 7:
            printf("Ves el color Blanco\n");
            break;


    }
    return EXIT_SUCCESS;
}
