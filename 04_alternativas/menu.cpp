#include <stdio.h>
#include <stdlib.h> 

//Función punto de entrada
int main(){
    int menu = 0;
    double base, altura;
    double resultado;

    system("clear");

    printf("Dime la figura (Número): \n");
    printf("1- Cuadrado\n"
           "2- Triangulo\n"
           "3- Circulo\n");
    scanf(" %i", &menu); 

    switch(menu) {
        case 1:
            printf("Dime la base: \n");
            scanf(" %lf", &base);
            printf("Dime la altura: \n");
            scanf(" %lf", &altura);
            resultado = altura * base;
            printf(" %lf\n", resultado);

            break;
    }

return EXIT_SUCCESS;
}
